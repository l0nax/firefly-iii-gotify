FROM golang AS builder

WORKDIR /app

COPY . .
ENV CGO_ENABLED=0 GOOS=linux GOARCH=amd64
RUN go build -ldflags="-extldflags=-static" -o /server .

######################
##    Res. Image    ##
######################
FROM cgr.dev/chainguard/wolfi-base

RUN apk add --no-cache --update-cache \
      ca-certificates

COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /server /server

ENTRYPOINT [ "/server" ]

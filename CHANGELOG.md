# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).


## 0.1.0 (2023-11-24)

### Other (1 change)
- Initial Release


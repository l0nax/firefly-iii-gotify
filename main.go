package main

import (
	"fmt"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/gofiber/fiber/v2"
)

var (
	secretToken string

	gotifyURL   string
	gotifyToken string
)

type TransactionType string

const (
	TransactionWithdrawal TransactionType = "withdrawal"
	TransactionDeposit    TransactionType = "deposit"
	TransactionTransfer   TransactionType = "transfer"
)

type Transaction struct {
	UUID     string `json:"uuid"`
	UserID   int    `json:"user_id"`
	Trigger  string `json:"trigger"`
	Response string `json:"response"`
	URL      string `json:"url"`
	Version  string `json:"version"`
	Content  struct {
		ID           int       `json:"id"`
		CreatedAt    time.Time `json:"created_at"`
		UpdatedAt    time.Time `json:"updated_at"`
		User         int       `json:"user"`
		GroupTitle   any       `json:"group_title"`
		Transactions []struct {
			User                         int             `json:"user"`
			TransactionJournalID         int             `json:"transaction_journal_id"`
			Type                         TransactionType `json:"type"`
			Date                         time.Time       `json:"date"`
			Order                        int             `json:"order"`
			CurrencyID                   int             `json:"currency_id"`
			CurrencyCode                 string          `json:"currency_code"`
			CurrencySymbol               string          `json:"currency_symbol"`
			CurrencyDecimalPlaces        int             `json:"currency_decimal_places"`
			ForeignCurrencyID            any             `json:"foreign_currency_id"`
			ForeignCurrencyCode          any             `json:"foreign_currency_code"`
			ForeignCurrencySymbol        any             `json:"foreign_currency_symbol"`
			ForeignCurrencyDecimalPlaces any             `json:"foreign_currency_decimal_places"`
			Amount                       string          `json:"amount"`
			ForeignAmount                any             `json:"foreign_amount"`
			Description                  string          `json:"description"`
			SourceID                     int             `json:"source_id"`
			SourceName                   string          `json:"source_name"`
			SourceIban                   any             `json:"source_iban"`
			SourceType                   string          `json:"source_type"`
			DestinationID                int             `json:"destination_id"`
			DestinationName              string          `json:"destination_name"`
			DestinationIban              string          `json:"destination_iban"`
			DestinationType              string          `json:"destination_type"`
			BudgetID                     any             `json:"budget_id"`
			BudgetName                   any             `json:"budget_name"`
			CategoryID                   any             `json:"category_id"`
			CategoryName                 any             `json:"category_name"`
			BillID                       any             `json:"bill_id"`
			BillName                     any             `json:"bill_name"`
			Reconciled                   bool            `json:"reconciled"`
			Notes                        any             `json:"notes"`
			Tags                         []string        `json:"tags"`
			InternalReference            any             `json:"internal_reference"`
			ExternalID                   string          `json:"external_id"`
			OriginalSource               string          `json:"original_source"`
			RecurrenceID                 any             `json:"recurrence_id"`
			BunqPaymentID                any             `json:"bunq_payment_id"`
			ImportHashV2                 string          `json:"import_hash_v2"`
			SepaCc                       any             `json:"sepa_cc"`
			SepaCtOp                     any             `json:"sepa_ct_op"`
			SepaCtID                     any             `json:"sepa_ct_id"`
			SepaDb                       any             `json:"sepa_db"`
			SepaCountry                  any             `json:"sepa_country"`
			SepaEp                       any             `json:"sepa_ep"`
			SepaCi                       any             `json:"sepa_ci"`
			SepaBatchID                  any             `json:"sepa_batch_id"`
			InterestDate                 any             `json:"interest_date"`
			BookDate                     any             `json:"book_date"`
			ProcessDate                  any             `json:"process_date"`
			DueDate                      any             `json:"due_date"`
			PaymentDate                  time.Time       `json:"payment_date"`
			InvoiceDate                  any             `json:"invoice_date"`
			Longitude                    any             `json:"longitude"`
			Latitude                     any             `json:"latitude"`
			ZoomLevel                    any             `json:"zoom_level"`
		} `json:"transactions"`
		Links []struct {
			Rel string `json:"rel"`
			URI string `json:"uri"`
		} `json:"links"`
	} `json:"content"`
}

func main() {
	app := fiber.New()

	secretToken = os.Getenv("SECRET_TOKEN")
	gotifyURL = os.Getenv("GOTIFY_URL")
	gotifyToken = os.Getenv("GOTIFY_TOKEN")

	if secretToken == "" || gotifyURL == "" || gotifyToken == "" {
		log.Fatal("SECRET_TOKEN cannot be empty!")
	}

	app.Post("/webhook", webhookHandler)
	app.Listen(":3000")
}

func webhookHandler(c *fiber.Ctx) error {
	var trans Transaction

	if err := c.BodyParser(&trans); err != nil {
		log.Printf("[ERROR] Unable to parse body: %+v\n", err)

		return c.SendStatus(http.StatusInternalServerError)
	}

	for _, tr := range trans.Content.Transactions {
		switch tr.Type {
		case TransactionDeposit:
			publishNotification("Einnahme",
				fmt.Sprintf("Du hast Geld von %s erhalten.", tr.SourceName),
				tr.Amount,
			)
		case TransactionWithdrawal:
			publishNotification("Ausgabe",
				fmt.Sprintf("Du hast Geld an %s gezahlt", tr.DestinationName),
				tr.Amount,
			)
		case TransactionTransfer:
			publishNotification("Zahlung",
				fmt.Sprintf("Du hast Geld an %s gesendet", tr.DestinationName),
				tr.Amount,
			)
		}
	}

	return c.SendStatus(http.StatusNoContent)
}

func publishNotification(title, msg, _ string) {
	http.PostForm(gotifyURL+"/message?token="+gotifyToken,
		url.Values{
			"title":   {title},
			"message": {msg},
		})
}
